[![Build Status](https://travis-ci.org/siaw23/monterail_meals.svg?branch=master)](https://travis-ci.org/siaw23/monterail_meals) [![Coverage Status](https://coveralls.io/repos/github/siaw23/monterail_meals/badge.svg?branch=master)](https://coveralls.io/github/siaw23/monterail_meals?branch=master)

### App URL

https://monterail.herokuapp.com/

### Instructions

# Meal ordering coordination system
We'd like you to write an application that coordinates meal ordering for Monterail employees.

Every day, circa 12:00AM, we decide where do we order our meal from and gather orders on Slack.

It's ineffective and sometimes orders are lost; that's why we would like to have an application to coordinate the process for us.

## Features
### 1. Authentication
Log in using OAuth (Facebook or GitHub).

### 2. Creating new order
Any user can start new order by providing name of the restaurant we order from.

### 3. Adding meal to order
Any user can add meal to started order by providing meal's name and price.
Only one item per user in order.

### 4. Changing order's status
It should be possible to change order's status:

- Finalized - no more items can be added
- Ordered
- Delivered

### 5. Viewing lists of all orders
It should be possible to see two lists of orders:

- Active - ordering is still in progress
- History - finalized and archived orders

## Constraints
- Application needs to be SPA (Single Page Application)
- Back-end part must be done in Ruby and front-end part in JavaScript or CoffeeScript
- There are no restrictions on Ruby or JavaScript framework choice
- Some tests are obligatory, but 100% coverage is not required

#### System dependencies
Ruby 2.3.1

Rials 4.2.6

PostgreSQL database for production deployment


#### Configuration
Copy `.env-template` to `.env` in the same directory and save with necessary information. Keep this format:

`SOME_KEY=appropriatevalue `

Create and configure your FaceBook app with steps below.
* Navigate to developers.facebook.com.
* Click “Add new app” under “My Apps” menu item.
* Click “Website” in the dialog.
* Click “Skip and Create ID”.
* Enter a name for your app and choose a category, click “Create”.
* You will be redirected to the app’s page. Click “Show” next to the “App Secret” and enter your password to reveal the key. Copy and paste those keys into your initializer file.
* Open “Settings” section.
* Click “Add Platform” and choose “Website”.
* Fill in “Site URL” (“http://localhost:3000” for local machine) and “App Domains” (must be derived from the Site URL or Mobile Site URL).
* Fill in “Contact E-mail” (it is required to make app active) and click “Save Changes”
* Navigate to the “Status & Review” section and set the “Do you want to make this app and all its live features available to the general public?” switch to “Yes”.

#### How to run the test suite
Simply run `rake test` from application root or subdirectories.


#### Deployment instructions
No specific steps.

`bundle install && rake db:create db:migrate && rails s`

###### Production
Make sure you have postgres installed.

Create and migrate production database: `RAILS_ENV=production rake db:create db:migrate`

Choose your WEB server and configure it to look up for Rails root directory.

Install environment variables from `.env-template` file.

Run `RAILS_ENV=production rake secret` and add SECRETE_KEY_BASE environment variable with the command output as its value.

Compile assets: `rake assets:precompile`

That's it from Rails side. Check out your WEB server documentation or tutorials on how to connect it with Rails.