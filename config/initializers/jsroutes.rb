JsRoutes.setup do |config|
  config.compact = true
  config.namespace = 'MOR'
  config.url_links = true
end